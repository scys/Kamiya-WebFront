let apiUrl = 'https://v1.kamiya.dev';

if(!window.location.href.match('kamiya.dev')) apiUrl = 'http://127.0.0.1:11681';

Sentry.init({
    dsn: "https://ac07660ce52b411e98c567054a0126c1@o4504624848896000.ingest.sentry.io/4504624878190592",
    // this assumes your build process replaces `process.env.npm_package_version` with a value
    integrations: [new Sentry.BrowserTracing()],

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
});